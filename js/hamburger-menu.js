let burgerBtn = document.querySelector(".burger-btn");

let nav = document.getElementsByTagName("nav")[0];

let ul = document.querySelector(".nav-first-ul");

let closeBtn = document.querySelector(".close-btn");

let active = document.querySelector(".active");

let dropdown = document.getElementById("dropdown");


function toggle() {
    ul.style.display = "block";
    nav.style.justifyContent = "flex-start";
    burgerBtn.style.display = "none";
    closeBtn.style.display = "block";
}

burgerBtn.onclick = toggle;



function close() {
    closeBtn.style.display = "none";
    ul.style.display = "none";
    burgerBtn.style.display = "block";
    nav.style.justifyContent = "flex-end";
}

closeBtn.onclick = close;


function toggleDropdown() {
    if (dropdown.style.display === "block") {
        dropdown.style.display = "none";
    } else {
        dropdown.style.display = "block";
        dropdown.style.visibility = "visible";
    }
}

active.onclick = toggleDropdown;


















